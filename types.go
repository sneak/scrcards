package main

// Card represents the structure of a card in the API response.
type Card struct {
	ID       string    `json:"id"`
	Slug     string    `json:"slug"`
	Name     string    `json:"name"`
	Hotscore int       `json:"hotscore"`
	Guardian Guardian  `json:"guardian"`
	Elements []Element `json:"elements"`
	Variants []Variant `json:"variants"`
}

// Guardian represents the structure of a guardian in the card details.
type Guardian struct {
	ID             string `json:"id"`
	Type           string `json:"type"`
	Rarity         string `json:"rarity"`
	TypeText       string `json:"typeText"`
	SubType        string `json:"subType"`
	RulesText      string `json:"rulesText"`
	Cost           int    `json:"cost"`
	Attack         *int   `json:"attack"`
	Defense        *int   `json:"defense"`
	Life           *int   `json:"life"`
	WaterThreshold int    `json:"waterThreshold"`
	EarthThreshold int    `json:"earthThreshold"`
	FireThreshold  int    `json:"fireThreshold"`
	AirThreshold   int    `json:"airThreshold"`
	CardID         string `json:"cardId"`
}

// Element represents the structure of an element in the card details.
type Element struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

// Variant represents the structure of a variant in the card details.
type Variant struct {
	ID         string  `json:"id"`
	Slug       string  `json:"slug"`
	Src        string  `json:"src"`
	Finish     string  `json:"finish"`
	Product    string  `json:"product"`
	Artist     string  `json:"artist"`
	FlavorText string  `json:"flavorText"`
	CardID     string  `json:"cardId"`
	SetCardID  string  `json:"setCardId"`
	SetCard    SetCard `json:"setCard"`
}

// SetCard represents the structure of a set card in the card details.
type SetCard struct {
	ID         string     `json:"id"`
	Slug       string     `json:"slug"`
	SetID      string     `json:"setId"`
	CardID     string     `json:"cardId"`
	Meta       MetaData   `json:"meta"`
	SetDetails SetDetails `json:"set"`
}

// MetaData represents the structure of meta data in the set card details.
type MetaData struct {
	ID             string `json:"id"`
	Type           string `json:"type"`
	Rarity         string `json:"rarity"`
	TypeText       string `json:"typeText"`
	SubType        string `json:"subType"`
	RulesText      string `json:"rulesText"`
	Cost           int    `json:"cost"`
	Attack         *int   `json:"attack"`
	Defense        *int   `json:"defense"`
	Life           *int   `json:"life"`
	WaterThreshold int    `json:"waterThreshold"`
	EarthThreshold int    `json:"earthThreshold"`
	FireThreshold  int    `json:"fireThreshold"`
	AirThreshold   int    `json:"airThreshold"`
	SetCardID      string `json:"setCardId"`
}

// SetDetails represents the structure of set details in the set card.
type SetDetails struct {
	ID          string `json:"id"`
	Name        string `json:"name"`
	ReleaseDate string `json:"releaseDate"`
}

// JSONPayload represents the structure of the nested JSON for the API request.
type JSONPayload struct {
	Query           string        `json:"query"`
	Sort            string        `json:"sort"`
	Set             string        `json:"set"`
	Filters         []interface{} `json:"filters"`
	Limit           int           `json:"limit"`
	VariantLimit    bool          `json:"variantLimit"`
	CollectionLimit bool          `json:"collectionLimit"`
	Cursor          int           `json:"cursor"`
	Direction       string        `json:"direction"`
}

// Input represents the top-level structure of the input JSON for the API request.
type Input struct {
	Part0 struct {
		JSON JSONPayload `json:"json"`
	} `json:"0"`
}
