# curiosa.io scraper

This is a scraper for the API at curiosa.io.

Included in the repository are json data files representing every SCR card.

# TODO / Known Bugs

* This does not include many of the promos, like the Boris Vallejo
  ones.

# License

WTFPL for `\*.go` files.
