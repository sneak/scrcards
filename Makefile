default: run

run:
	go run *.go

fmt:
	go fmt *.go
	prettier -w data
